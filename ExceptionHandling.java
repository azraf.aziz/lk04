import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");
        boolean validInput = false;
        do {
            try{
                System.out.print("Masukkan Pembilang : ");
                int a = scanner.nextInt();
                System.out.print("Masukkan Penyebut : " );
                int b = scanner.nextInt();
                int result = pembagian(a, b);
                System.out.println("Hasil : "+result);
                //edit here
                validInput = true;
            }catch(InputMismatchException e){
                System.out.println("Input harus bilangan bulat");
                scanner.nextLine();
            }catch(ArithmeticException e){
                System.out.println("Bilangan penyebut tidak boleh angka 0");
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        // add exception apabila penyebut bernilai 0
        if(penyebut == 0){
            throw new ArithmeticException();
        }
        return pembilang / penyebut;
    }
    
}
